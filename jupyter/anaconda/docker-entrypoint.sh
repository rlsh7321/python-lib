#!/bin/bash
function jupyter_passwd() {
  python <<-EOF
from notebook.auth import passwd
if '$1' == '':
  print(passwd())
else:
  print(passwd('$1'))
EOF
}

source ~/.bashrc
mkdir -p ~/.jupyter
cat <<-EOF > ~/.jupyter/jupyter_notebook_config.json
{
  "JupyterApp": {
    "answer_yes": true
  },
  "NotebookApp": {
    "ip": "0.0.0.0",
    "port": 8888,
    "password": "$(echo -n $(jupyter_passwd "$JUPYTER_PASSWORD"))",
    "notebook_dir": "/home/ubuntu/jupyter",
    "open_browser": false
  }
}
EOF
jupyter lab $@
