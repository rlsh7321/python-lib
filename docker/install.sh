#!/bin/bash
if [ $UID -ne 0 ]; then
  echo '* Root permission required.' 1>&2
  exit -1
fi

groupadd docker -f
DOCKER_LIB=/usr/local/lib/docker
DOCKER_CONFIG=/etc/docker
mkdir -p $DOCKER_LIB $DOCKER_CONFIG
cp -r bin cli-plugins etc systemd $DOCKER_LIB
ln -s $DOCKER_LIB/bin/* /usr/bin
ln -s $DOCKER_LIB/etc/* $DOCKER_CONFIG
ln -s $DOCKER_LIB/systemd/* /etc/systemd/system
systemctl daemon-reload
systemctl enable --now docker.socket docker.service containerd.service
