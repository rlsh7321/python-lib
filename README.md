# 1. 시작

```bash
＃> git clone https://gitlab.com/rlsh7321/python-lib.git python-lib
＃> cd python-lib
```

<br>

# 2. Docker 설치

```bash
＃> bash -c 'cd docker && bash install.sh'
```

<br>

# 3. Jupyter 설치

```bash
＃> bash -c 'cd jupyter && bash install.sh'
```
